---
layout: idea
title:  "JSON Assertor - JMeter Plugin"
description: 'JSON Assertor - JMeter Plugin'
image: /images/big-back-1.jpg
categories: jmeter plugins
tags: 
    - automation
    - jmeter
    - plugins
    - json
    - assertion
author: rsakhale
---

JMeter supports assertions with the help of regex extraction, but having JSON Assertor would make it more efficient as we can straight use them for REST service assertion with the help of JSON Path
<!--more-->
### Types of Assertion

1. Full JSON Body Assertion
2. Partial JSON Assertion with the help of JSONPath
